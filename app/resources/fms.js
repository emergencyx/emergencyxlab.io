import FMS from './components/fms.svelte';
import './scss/fms.scss';
import './scss/fms-alternative.scss';
import './scss/fms-lahmy.scss';
import './scss/fms-alex.scss';
import './scss/fms-bfrf.scss';

const urlParams = new URLSearchParams(window.location.search);
const fms = new FMS({
    target: document.getElementById('fms'),
    props: {
        sessionId: urlParams.get('id')
    }
});

export default fms;
