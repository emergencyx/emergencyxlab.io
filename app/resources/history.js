import HistoryBrowser from './components/history-browser.svelte';

const historyService = "localhost:8000/"

const historyBrowser = new HistoryBrowser({
    target: document.getElementById('history'),
    props: {
        api: {
            history: historyService
        },
    }
});

export default historyBrowser;
