import ServerBrowser from './components/server-browser.svelte';

const serverBrowser = new ServerBrowser({
    target: document.getElementById('serverbrowser'),
    props: {
        sessions: [],
        api: {
            matchmaking: 'wss://venus.emergencyx.de/websocket'
        }
    }
});

export default serverBrowser;
