---
type: "explorer"
title: 'EmergencyX | Emergency Explorer Download'
---
# Emergency Explorer

Der Emergency Explorer ist ein Modifikations-Verwaltungs- und Hilfs-Tool von EmergencyX.

## Was ist der Emergency Explorer überhaupt?

Der Emergency Explorer ist ein neuer Mod-Installer.

Man könnte sogar, etwas clickbaity, sagen: Der erste cross plattform Emergency Mod-Installer. Normalerweise versteht man unter “cross plattform” die Lauffähigkeit unter verschiedenen Betriebssystemen. Hier ist damit aber hauptsächlich gemeint, dass der Emergency Explorer nicht nur Emergency 5 Modifikationen, sondern auch auch Emergency 4 Modifikationen installieren kann.

Große Modifikationen, wie Wuppertal und Bieberfelde, haben deutlich gezeigt, dass der aktuelle Mod-Installer alles andere als zuverlässig funktioniert, daher wurde der Emergency Explorer in seiner aktuellen Version mit genau diesem Gedanken entwickelt.

Der Emergency Explorer ist aber mehr als ein simpler Mod-Installer. Er bietet Spielern einen einfacheren Zugang zu Support bei Problemen mit Mods, lässt sie ihre Emergency 5 Screenshots verwalten und darüber hinaus die Einstellungen des Spiels ändern.

{{< rawhtml >}}
<a class="btn btn-primary" href="https://clientcdn.emergencyx.de/latest" role="button">Download</a>
{{< /rawhtml >}}


## Funktionsübersicht

### Emergency 5

- Modifikationen installieren (ZIP-Dateien)
- Modifikationen installieren (aus dem Web herunterladen)
- Modifikationen verwalten (Installieren, löschen, Reihenfolge ändern, (de)aktivieren)
- Direkt in den Editor starten
- Spiel starten
- Diverse Verzeichnisse öffnen

Im Normalfall wird der Installationsordner von EM5/EM2016/EM2017/EM20Y automatisch gefunden. Dann können: das Spiel gestartet werden, der Editor direkt geladen werden und diverse Verzeichnisse auf der Festplatte geöffnet werden.

Natürlich lassen sich Emergency 5-Modifikationen als ZIP-Datei von der Festplatte installieren.

Es gibt außerdem einen ModStore, der die Möglichkeit bietet Modifikationen zum Download (und Aktualisieren) anzubieten. Eine genauere Erläuterung findet sich im “Anhang”.

Emergency 5 bietet die Möglichkeit Mods zu aktivieren, deaktivieren, in der Ladereihenfolge zu priorisieren oder zu löschen. Dies kann der Emergency Explorer ebenfalls. Für Modder besteht außerdem die Möglichkeit über ein Vergleichs-Tool Patche zu erstellen um ein kleiners Update-Paket zwischen 2 Versionen zu erstellen.

![Picture of the Emergency Explorer Dashboard, showing a list of supported games](images/1_Dashboard.jpg)

![Picture of the Emergency 5 Modification Overview in Emergency Explorer](images/2_Mods_Em5.jpg)

![Picture of the Emergency Explorer ModStore, allowing direct download of Modifications](images/8_Mods_installieren.jpg)

Neben diesen Kernfunktionalitäten, wird für Emergency 5 noch weiteres geboten:

- TIFF-Screenshots als JPG/PNG oder GIF exportieren
- EM5-Cache leeren
- Eine Support-Datei erstellen
- Spieleinstellungen verwalten

Die “hauseigene” Screenshot-Funktionalität von Emergency 5 speichert Screenshots als unhandliches TIFF-Format und legt diese an einem obskuren Ort ab. Der Emergency Explorer lässt die erstellten Screenshots nicht nur als Vorschau anzeigen, sondern erlaubt auch den Export in gängige Formate, wie JPG oder PNG.

Die Funktionalität um den Cache des Spiels zu löschen, die ich schon vor längerem einmal als eigenständiges Tool entwickelt hatte, wurde ebenfalls in dieses Programm übernommen.

Die Möglichkeiten von EM5 haben sich stark erweitert. Damit aber natürlich auch die Komplexität, wenn einmal etwas schieflaufen sollte. Dafür gibt es im Emergency Explorer einen Support-Assistent. Über den Menüpunkt “Support-Datei erstellen”, wird eine ZIP-Datei erstellt, in die zunächst eine DxDiag-Datei geladen wird. Danach bekommt der Benutzer die Auswahl eine oder mehrere Em5-Log-Dateien mit anzufügen. Außerdem wird die aktuelle Version der Emergency 5 Einstellungen hinzugefügt. World-Of-Emergency Account und Passwort werden dabei aber zuvor aus der Datei herausgefiltert.

Einige Einstellungen, welche über Emergency 5 selbst nicht zu bearbeiten sind, können über den Emergency Explorer geändert werden. Weitere Einstellungen lassen sich selbstverständlich zum Bearbeiten hinzufügen.

![Picture of screenshot overview in Emergency Explorer](images/3_Mods_Screenshots.jpg)

![Picture of the Emergency 5 cache deletion tool in Emergency Explorer](images/4_Cache_Em5.jpg)

![Picture of the Support-Pack-Creator for Emergency 5 in Emergency Explorer](images/5_Support_Em5.jpg)

![Picture of the possibilities to change Emergency 5 settings in Emergency Explorer](images/6_Einstellungen_Em5.jpg)

### Emergency 4

Aktuell wird für Emergency 4 nur das Installieren, Auflisten und Löschen von Modifikationen unterstützt. Modifikationen für Emergency 4 werden oft als .e4mod-Dateiformat veröffentlicht, das von üblichen Archiv-Programmen nicht unterstützt wird. Der Emergency Explorer unterstützt eine experimentelle Installation von .e4mod und .zip Dateien.

Kleiner Bonus: Modifikationen, die via Emergency Explorer installiert werden, werden von Emergency 4 nicht als beschädigt angezeigt.

Stand jetzt ist die Unterstützung für Emergency 4 sehr eingeschränkt. In Zukunft  könnten auch hier mehr Funktionen folgen, wie etwa Änderung von Emergency 4 - Einstellungen wie Bildschirmauflösung.

![Picture of screenshot overview in Emergency Explorer](images/7_Mods_Em4.jpg)

## Sonstiges

Der Emergency Explorer hat einen Dark-Mode. Dieser r ichtet sich nach den System-Einstellunge. Kann aber auch individuell eingestellt werden.

![Picture of screenshot overview in Emergency Explorer](images/9_Dark.jpg)

## Fragen und Antworten

### F: Mein Steam oder Spielpfad sind unter C:\Program Files - was gibt es zu beachten?
A: Da Windows allem in C:\Program Files einen speziellen Schutzstatus zuspricht müssen, bei Mod-Installation für EM4 oder Mod-Installation für EM5 in das Data-Verzeichnis, Administrator Privilegien zur Ausführung des Emergency Explorers verwendet werden.

### F: Emergency Explorer hat eine Emergency Installation automatisch erkannt - kann ich eine andere verwenden?
A: Ja! Dafür lässt sich in den Einstellungen ein Pfad angeben, der die Detection überschreibt. Dieser muss auch verwendet werden, wenn keine Installation gefunden werden konnte.

### F: Welche Technologie wird verwendet?
A: Der Emergency Explorer wurde in C# und XAML geschrieben, basierend auf .NET und Avalonia UI.

### F: Gibt es spezielle Vorraussetzungen?
A: Nein. Die Unterstützung von Windows 7 wird aber bspw. nicht garantiert. Offiziell Unterstützt werden erst Windows Betriebssystem ab Version 8.1.

### F: Kann man sich an der Entwicklung beteiligen, Fehler melden oder Vorschläge einreichen?
A: Ja! Vorschläge und Fehler können hier gemeldet werden. Es können aber auch Fehler in unserem GitLab Repo eingetragen werden, welches unter https://gitlab.com/emergencyx/Client erreichbar ist. Hier kann sich gerne an der Entwicklung beteiligt werden.

### F: Was genau wird in die Support-Datei eingefügt?
A: 1. Der Output des DxDiag Befehls (dies hat denselben Effekt, wie wenn man das DxDiag Programm selbst aufruft und die Informationen abspeichert). 2. Die Em5-Log-Dateien, welche man selbstständig auswählt. 3. eine aktuelle  Version der emergency_5_settings.json aus %AppData%\Promotion Software GmbH\EMERGENCY 5. Dabei werden aber der World of Emergency Account-Name sowie das Passwort entfernt, sodass diese nicht mit übertragen werden!

### F: Was hat es mit diesem Dienst auf sich, der Modifikationen aus dem Internet herunterlädt?
A: Dieser Dienst soll es erlauben, Modifikationen ohne Umweg über die WebDisk herunterzuladen und direkt zu installieren. In Zukunft sollen damit auch große Modifikationen die Möglichkeit bekommen, kleinere Update-Pakete zu releasen. EmergencyX kontrolliert dabei aber nicht den Server auf dem die Modifikationen liegen (müssen). Vielmehr verhält sich der EmergencyX Dienst (Phonebook oder Telefonbuch, wie ich ihn nenne) nur wie ein Verzeichnis. In dieses Verzeichnis lassen sich Download-Quellen für Mods eintragen. Entweder ein zentraler Mirror, oder mehrere für Balancing.
Der EmergencyX Dienst liefert also eine Information, welche Modifikationen sich in das Verzeichnis eingetragen haben und wie der Emergency Explorer diese Dateien von den Servern der Modding-Teams herunterladen kann.
Im ersten Schritt wird aktuell nur die Bieberfelde Next Gen-Modifikation ausgeliefert. Wir laden aber andere Modifikationen ein, sich ebenfalls integrieren zu lassen.

Hier könnt ihr die aktuelle Beta herunterladen und ausprobieren. Wir haben uns nach eigenen Tests entschieden, diese Version für die Öffentlichkeit freizugeben und sind auf eure Erfahrungen und Vorschläge gespannt.
