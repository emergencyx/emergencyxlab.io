Mit EmergencyX versuchen wir das Modding und Ökosystem der Emergency-Spielereihe zu verbessern.
Viele dieser Projekte sind Open Source, wir nutzen [GitLab](https://gitlab.com/emergencyx) als Plattform für unseren Code und freuen uns über Feedback oder Helfer.

## Emergency Explorer
Der Emergency Explorer ist ein Mod-Installer für Emergency 4 und 5. Er erleichtert die Verwaltung und Installation von Modifikationen, bietet eine Plattform zum Download von neuen Modifikationen und Updates, und hilft beim Support mit Problemen im Spiel.

{{< rawhtml >}}
<a class="btn btn-primary" href="/explorer/" role="button">Zum Download</a>
{{< /rawhtml >}}

## Emergency 4
### Matchmaking
Wir kümmern uns um das Matchmaking für Emergency 4. Unser Server vermittelt Sessions zwischen Hosts und Mitspielern.
Eine Übersicht über alle laufenden Spiele in Echtzeit ist in unserem [Serverbrowser](https://www.emergencyx.de/multiplayer/)
auch außerhalb vom Spiel sichtbar.

### FMS
Ein mit der Bieberfelde (und Submodifikationen) kompatibles FMS findest du [hier](https://www.emergencyx.de/fms/).
Zum Spielen wird der dem Download beigelegte LogSyncer benötigt.

## Emergency 5
### MMO für Emergency 5
Das EmergencyX MMO ist eine hybride Modifikation für Emergency 5. Die Modifikation erweitert Emergency 5 um eine Management-Komponente, die auch von unterwegs aus auf dem Smartphone bedienbar ist
- Erweitere deine Garage und schaffe neue Fahrzeuge an, die die Bewältigung von Einsätzen im Spiel erleichtern
- Starte mit einem beschränkten Pool von Fahrzeugen, arbeite Einsätze ab um Credits zu verdienen und neue Fahrzeuge freizuschalten
- Wenn dir ein benötigtes Fahrzeug fehlt, leihe es von anderen Mitspielern aus
- Stelle deine eigenen Fahrzeuge anderen Mitspielern und virtuellen Einsätzen zur Verfügung, die mit Credits entlohnt werden

