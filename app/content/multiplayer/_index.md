---
type: "multiplayer"
aliases:
    - "/multiplayer/browser/emergency-4"
title: 'EmergencyX | Emergency 4 Multiplayer Server Browser'
---
Der Serverbrowser zeigt alle laufenden Multiplayerspiele für Emergency 4 an.
Auf der Seite für [historische Aktivität]({{< ref "history" >}}) führen wir Statistiken für beliebte Modifikationen
und generell Aktivität im Multiplayer.

Änderungen bei den Internetanbietern bereiten zunehmend Probleme
bei der Teilnahme an Spielen, deshalb prüft unser Matchmaking-Server, ob die
nötigen Ports korrekt freigegeben wurden und die Session erreichbar ist.
