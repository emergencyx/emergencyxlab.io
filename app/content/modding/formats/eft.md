---
layout: "formats"
title: ".eft"
---
Bodentexturen werden im *Emergency Texture Format* gespeichert.

## Tools
- https://emergency-forum.de/index.php?thread/70181-update-1-3-eftxplorer-eft-converter-viewer/
- https://github.com/annabelsandford/EFTXplorer
