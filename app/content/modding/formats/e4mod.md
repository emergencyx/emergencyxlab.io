---
layout: "formats"
title: ".e4mod"
---
Das .e4mod Format wird vom Mod-Installer verwendet.
Es komprimiert die benötigten Dateien einer Modifikation und verpackt sie mit Metainformationen zur Installation. 

## Tools
- https://gitlab.com/emergencyx/em4/e4mod
- https://gitlab.com/emergencyx/Client/-/blob/master/EmergencyX%20Client/EmergencyX/EmergencyX.Emergency4/Logic/ModPackageExtractor.cs
- https://emergency-forum.de/filebase/index.php?entry/2542-emergency-4-modinstaller-quellcode/
