---
layout: "formats"
title: ".e4m"
---
Das .e4m beschreibt wo sich welche Objekte auf einer Karte befinden und ihre Eigenschaften.
Die Bodentextur für die Karte ist nicht mit in der .e4m gespeichert, sondern befinden sich in einer [.eft Datei]({{< ref "eft" >}} "Bodentextur").
Höheninformationen für den Boden der Karte wiederum sind ein Teil der .e4m.

## Tools
- https://gitlab.com/emergencyx/em4/e4m
