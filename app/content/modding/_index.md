---
type: "modding"
layout: "single"
title: "EmergencyX | Modding"
---
Im diesem Modding-Bereich finden sich Inhalte zum Modding von Emergency 4.

## Dateiformate
Die Seite [Dateiformate]({{< ref "formats" >}} "Dateiformate") bietet eine Übersicht über
alle in Emergency 4 verwendeten Dateiformate und ihre (bekannten) Inhalte.
