# emergencyx.de
The home for our emergencyx.de website 🥳

## app/
For the static HTML frontend.
```
yarn install
yarn run build
hugo serve
```

## services/
### matchmaking
Service for historic multiplayer activity.

### FMS
Service for the FMS: Report vehicle state in realtime.

