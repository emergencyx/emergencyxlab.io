# Services
This directory hosts code for modularized services.
The services are implemented in Go and share their dependencies.
The dependencies are vendored into the `vendor/` directory.
Make sure to `go mod tidy` and `go mod vendor` to keep them up to date whenever something was updated or changed.
Services offer only API: Any UI currently lives in the top-level webapp in `app/` and is maintained independently.

Every service must have its entrypoint in `cmd/<service>`. It should also register in the `Makefile`, e.g. to `make history` for the history service.
Services share the `pkg/runtime`: It standarizes setup of the API servers, e.g. with environment setup, logging, healthcheck, and lifecycle management.
APIs are placed in `api/` and come versioned, e.g. `history_v1` for the first version of the history service API.

Code in `pkg/` should be written in such way that it can be reused by all services,
one such example might be the `pkg/runtime` but also more general utilities to parse requests.
