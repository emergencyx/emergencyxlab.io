package history_v1

import (
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"emergencyx.gitlab.io/pkg/history"
	"emergencyx.gitlab.io/pkg/util"
)

func RegisterWithRouter(router *gin.RouterGroup, store history.SessionStorage) {
	router.GET("/", getSessions(store))
	router.GET("/monthly", getStats(store))
	router.GET("/mod", getStatsForMod(store))
	router.GET("/heat", getActivity(store))
	router.GET("/top", getLeaders(store))
}

func getSessions(store history.SessionStorage) gin.HandlerFunc {
	return func(c *gin.Context) {
		date, err := util.QueryDate(c, "date")
		if err != nil {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		sessions, err := store.GetSessions(c, date)
		if err != nil {
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		c.JSON(http.StatusOK, sessions)
	}
}

func getLeaders(store history.SessionStorage) gin.HandlerFunc {
	return func(c *gin.Context) {
		n, err := util.QueryLimit(c, "limit", 10, 25)
		if err != nil {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}

		stats, err := store.GetLeaderboard(c, 7*24*time.Hour, time.Now(), n)
		if err != nil {
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		c.JSON(http.StatusOK, stats)
	}
}

func getActivity(store history.SessionStorage) gin.HandlerFunc {
	return func(c *gin.Context) {
		stats, err := store.GetPlayerActivity(c, time.Now())
		if err != nil {
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		c.JSON(http.StatusOK, stats)
	}
}

func getStatsForMod(store history.SessionStorage) gin.HandlerFunc {
	return func(c *gin.Context) {
		to := time.Now()
		since := to.AddDate(0, 0, -14)

		mod, _ := c.GetQuery("mod")
		stats, err := store.GetStatisticsByMod(c, since, to, mod)
		if err != nil {
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		c.JSON(http.StatusOK, stats)
	}
}

func getStats(store history.SessionStorage) gin.HandlerFunc {
	return func(c *gin.Context) {
		from, err := util.QueryMonth(c, "from")
		if err != nil {
			_ = c.AbortWithError(http.StatusBadRequest, err)
			return
		}
		var to time.Time
		if _, exists := c.GetQuery("to"); exists {
			// If it was set, let's parse and err out if badly formatted.
			t, err := util.QueryMonth(c, "to")
			if err != nil {
				_ = c.AbortWithError(http.StatusBadRequest, err)
				return
			}
			to = t
		} else {
			to = from
		}

		stats, err := store.GetStatistics(c, from, to)
		if err != nil {
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		c.JSON(http.StatusOK, stats)
	}
}
