package fuenf_v1

import (
	"log/slog"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"

	"emergencyx.gitlab.io/pkg/fuenf"
)

func RegisterWithRouter(router *gin.RouterGroup, b *fuenf.Broadcast) {
	router.GET("/", handleBroadcast(b))
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  256,
	WriteBufferSize: 1024,
}

func handleBroadcast(b *fuenf.Broadcast) gin.HandlerFunc {
	return func(g *gin.Context) {
		c, err := upgrader.Upgrade(g.Writer, g.Request, nil)
		if err != nil {
			slog.With("error", err).Error("failed to handle upgrade")
			return
		}

		go func() {
			b.Subscribe(c)
			for {
				if _, _, err := c.NextReader(); err != nil {
					c.Close()
					break
				}
			}
			b.Unsubscribe(c)
		}()
	}
}
