package history_v2

import (
	"log/slog"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"

	"emergencyx.gitlab.io/pkg/history"
)

type apiRequest struct {
	Name         string `json:"name"`
	Numpl        int    `json:"numpl"`
	Maxpl        int    `json:"maxpl"`
	Mod          string `json:"mod"`
	Nation       string `json:"nation"`
	Started      bool   `json:"started"`
	Password     bool   `json:"password"`
	Connectivity bool   `json:"connectivity"`
	Id           string `json:"id"`
	Modem        bool   `json:"modem"`
	CreatedAt    string `json:"created_at"`
	EndedAt      string `json:"ended_at"`
}

func RegisterWithRouter(router *gin.RouterGroup, store history.SessionStorage) {
	router.POST("/", addSession(store))
}

func addSession(store history.SessionStorage) gin.HandlerFunc {
	return func(c *gin.Context) {
		var input apiRequest
		if err := c.BindJSON(&input); err != nil {
			return
		}
		slog.With(slog.Any("raw", input)).Info("received request")

		var session history.Session
		createdAt, err := time.Parse(time.RFC3339, input.CreatedAt)
		if err != nil {
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
		endedAt, err := time.Parse(time.RFC3339, input.EndedAt)
		if err != nil {
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}
		session.CreatedAt = createdAt
		session.EndedAt = endedAt
		session.Mod = input.Mod
		session.Name = input.Name
		session.PlayerCount = input.Numpl
		session.MaxPlayerCount = input.Maxpl
		session.Connectivity = input.Connectivity
		session.Password = input.Password
		slog.With(slog.Any("parsed", session)).Info("parsed request")

		_, err = store.AddSession(c, session)
		if err != nil {
			_ = c.AbortWithError(http.StatusInternalServerError, err)
			return
		}

		c.Status(http.StatusCreated)
	}
}
