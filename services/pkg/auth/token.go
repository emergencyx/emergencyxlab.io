package auth

import (
	"crypto/subtle"
	"net/http"

	"github.com/gin-gonic/gin"
)

const tokenHeader = "Authorization"

func WithAppToken(token string) gin.HandlerFunc {
	tokenBytes := []byte(token)

	return func(c *gin.Context) {
		t := c.GetHeader(tokenHeader)

		if subtle.ConstantTimeCompare(tokenBytes, []byte(t)) != 1 {
			c.AbortWithStatus(http.StatusUnauthorized)
			return
		}

		c.Next()
	}
}
