package login

import (
	"bufio"
	"log/slog"
	"net/http"
	"net/url"
	"os"
	"strings"
	"time"
)

func (h *Handler) setRedirectCookie(w http.ResponseWriter, r *http.Request) {
	redirectTo := r.URL.Query().Get(h.config.RedirectQueryParameter)
	if redirectTo != "" && h.allowRedirect(r) && r.Method != "POST" {
		cookie := http.Cookie{
			Name:  h.config.RedirectQueryParameter,
			Value: redirectTo,
		}
		http.SetCookie(w, &cookie)
	}
}

func (h *Handler) deleteRedirectCookie(w http.ResponseWriter, r *http.Request) {
	_, err := r.Cookie(h.config.RedirectQueryParameter)
	if err == nil {
		cookie := http.Cookie{
			Name:    h.config.RedirectQueryParameter,
			Value:   "delete",
			Expires: time.Unix(0, 0),
		}
		http.SetCookie(w, &cookie)
	}
}

func (h *Handler) allowRedirect(r *http.Request) bool {
	if !h.config.Redirect {
		return false
	}

	if !h.config.RedirectCheckReferer {
		return true
	}

	referer, err := url.Parse(r.Header.Get("Referer"))
	if err != nil {
		slog.Error("failed to redirect", "error", err)
		return false
	}
	if referer.Host != r.Host {
		slog.Warn("redirect from referer does not matching current domain", "referer", referer.Host, "current", r.Host)
		return false
	}
	return true
}

func (h *Handler) redirectURL(r *http.Request, w http.ResponseWriter) string {
	targetURL, foundTarget := h.getRedirectTarget(r)
	if foundTarget && h.config.Redirect {
		sameHost := targetURL.Host == "" || r.Host == targetURL.Host
		if sameHost && targetURL.Path != "" {
			return targetURL.Path
		}
		if !sameHost && h.isRedirectDomainWhitelisted(r, targetURL.Host) {
			return targetURL.String()
		}
	}
	return h.config.SuccessURL
}

func (h *Handler) getRedirectTarget(r *http.Request) (*url.URL, bool) {
	cookie, err := r.Cookie(h.config.RedirectQueryParameter)
	if err == nil {
		url, err := url.Parse(cookie.Value)
		if err != nil {
			slog.Warn("error parsing redirect URL", "error", err)
			return nil, false
		}
		return url, true
	}

	// try reading parameter as it might be a POST request and so not have set the cookie yet
	redirectTo := r.URL.Query().Get(h.config.RedirectQueryParameter)
	if redirectTo == "" || r.Method != "POST" {
		return nil, false
	}
	url, err := url.Parse(redirectTo)
	if err != nil {
		slog.Warn("error parsing redirect URL", "error", err)
		return nil, false
	}
	return url, true
}

func (h *Handler) isRedirectDomainWhitelisted(_ *http.Request, host string) bool {
	if h.config.RedirectHostFile == "" {
		slog.Warn("no redirect whitelist file given", "host", host)
		return false
	}

	f, err := os.Open(h.config.RedirectHostFile)
	if err != nil {
		slog.Warn("can't open redirect whitelist domains file", "file", h.config.RedirectHostFile)
		return false
	}
	defer f.Close()
	scanner := bufio.NewScanner(f)
	scanner.Split(bufio.ScanLines)
	for scanner.Scan() {
		if host == strings.TrimSpace(scanner.Text()) {
			return true
		}
	}
	slog.Warn("redirect attempt but not in redirect whitelist", "host", host)
	return false
}
