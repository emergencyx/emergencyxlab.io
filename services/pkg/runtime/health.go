package runtime

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type HealthProvider interface {
	// HealthIncludeData allows to include additional information in the health endpoint.
	HealthIncludeData() any
}

// HealthProviders is not exactly safe to use but easy to implement like this.
// It allows pluggable providers to include additional information in the output.
var HealthProviders []HealthProvider

type healthResponse struct {
	Status string `json:"status"`
	Extra  []any  `json:"extra"`
}

func GetHealth(c *gin.Context) {
	res := healthResponse{Status: "🚀"}
	if len(HealthProviders) > 0 {
		res.Extra = make([]any, 0, len(HealthProviders))
		for _, provider := range HealthProviders {
			res.Extra = append(res.Extra, provider.HealthIncludeData())
		}
	}
	c.JSON(http.StatusOK, res)
}
