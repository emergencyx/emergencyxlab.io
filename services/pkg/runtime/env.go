package runtime

import (
	"fmt"
	"os"
)

// IsDebug scans the environment for the 'DEBUG' environment variable.
func IsDebug() bool {
	_, debug := os.LookupEnv("DEBUG")
	return debug
}

// Address tries to read the address to bind to from the environment.
func Address() string {
	v, ok := os.LookupEnv("ADDRESS")
	if !ok {
		return ":8000"
	}
	return v
}

// MustHaveEnv returns the value for the environment variable env.
// It panics is the variable is not present.
func MustHaveEnv(env string) string {
	v, exists := os.LookupEnv(env)
	if !exists {
		panic(fmt.Sprintf("required env '%s' is not present", env))
	}
	return v
}
