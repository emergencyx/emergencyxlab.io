package runtime

import (
	"context"
	"errors"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"runtime/debug"
	"time"

	"github.com/gin-contrib/gzip"
	"github.com/gin-gonic/gin"
	sloggin "github.com/samber/slog-gin"
)

const (
	LogLevelEnv = "LOGLEVEL"
)

func SetupLogging() {
	level := slog.LevelInfo
	if IsDebug() {
		// bump to debug if we know this already
		level = slog.LevelDebug
	}
	if s, exists := os.LookupEnv(LogLevelEnv); exists {
		// overwrite if given
		_ = level.UnmarshalText([]byte(s))
	}

	defaultLogger := slog.New(slog.NewTextHandler(os.Stdout, &slog.HandlerOptions{Level: level}))

	revision := getRevision()
	if revision != "" {
		defaultLogger = defaultLogger.With("release", revision)
	} else {
		defaultLogger.Info("no build information available")
	}
	slog.SetDefault(defaultLogger)
}

// getRevision tries to return the revision if provided by the VCS build info.
func getRevision() string {
	build, ok := debug.ReadBuildInfo()
	if ok {
		// Let's find the embedded build information, whey
		for _, setting := range build.Settings {
			if setting.Key == "vcs.revision" {
				return setting.Value[:8]
			}
		}
	}
	return ""
}

func NewEngineWithDefaults() *gin.Engine {
	router := gin.New()
	gin.DisableConsoleColor()

	router.Use(sloggin.New(slog.With("component", "gin")))
	router.Use(gin.Recovery())
	router.Use(gzip.Gzip(gzip.DefaultCompression))

	if IsDebug() {
		router.Use(func(c *gin.Context) {
			c.Header("Access-Control-Allow-Origin", "*")
		})
	}

	router.GET("/health", GetHealth)
	return router
}

func Forever(router *gin.Engine, logger *slog.Logger) {
	address := Address()
	logger.With("address", address).Error("start HTTP runtime")

	srv := &http.Server{
		Handler:      router,
		Addr:         address,
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	go func() {
		if err := srv.ListenAndServe(); !errors.Is(err, http.ErrServerClosed) {
			logger.With("error", err).Error("failed to stop server")
		}
		logger.Info("server terminated")
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	<-c
	logger.Info("shutting down")

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	_ = srv.Shutdown(ctx)

	logger.Info("shutdown complete")
	os.Exit(0)
}
