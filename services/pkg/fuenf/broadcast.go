package fuenf

import (
	"encoding/json"
	"sync"

	"github.com/gorilla/websocket"
)

type Broadcast struct {
	mu          sync.Mutex
	clients     map[*websocket.Conn]struct{}
	lastMessage *websocket.PreparedMessage
}

func NewBroadcast() *Broadcast {
	return &Broadcast{
		clients: make(map[*websocket.Conn]struct{}),
	}
}

func (b *Broadcast) Subscribe(c *websocket.Conn) {
	b.mu.Lock()
	defer b.mu.Unlock()

	b.clients[c] = struct{}{}

	// Try to do a fast announcement
	if b.lastMessage != nil {
		err := c.WritePreparedMessage(b.lastMessage)
		if err != nil {
			delete(b.clients, c)
			c.Close()
		}
	}
}

func (b *Broadcast) Unsubscribe(c *websocket.Conn) {
	b.mu.Lock()
	defer b.mu.Unlock()

	delete(b.clients, c)
}

func (b *Broadcast) Announce(sessions Sessions) {
	b.mu.Lock()
	defer b.mu.Unlock()

	j, _ := json.Marshal(sessions)
	message, err := websocket.NewPreparedMessage(websocket.TextMessage, j)
	if err != nil {
		return
	}

	for c := range b.clients {
		err := c.WritePreparedMessage(message)
		if err != nil {
			delete(b.clients, c)
			c.Close()
		}
	}

	b.lastMessage = message
}
