package fuenf

type Mod struct {
	Name        string `json:"Name"`
	Version     string `json:"Version"`
	Author      string `json:"Author"`
	Description string `json:"Description"`
}

func (m Mod) asStringForMultiplayer() string {
	return m.Name + m.Version + m.Author
}
