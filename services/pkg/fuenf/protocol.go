package fuenf

import (
	"bytes"
	"encoding/binary"
	"errors"
	"strings"
	"unicode"

	"emergencyx.gitlab.io/pkg/util"
)

// Establishment and session handling
var (
	EstablishIn    = util.MustDecode("0000000001000000aed844ba")
	EstablishReply = util.MustDecode("00000000aed844ba")

	// ListPrefix is the prefix for a request to receive packets.
	ListPrefix = []byte{0x80, 0x01, 0x00, 0x01, 0x00, 0x92, 0xee, 0x7a, 0x4b}

	// SessionsPrefix is the shared prefix for a session list in all observed packets so far, regardless the number of sessions.
	SessionsPrefix = []byte{0x80, 0x01, 0x00, 0x01, 0x00, 0xb7, 0x43, 0xa2, 0x48}
	// sessionPrefix is a hack around finding they index of each packet. The delimiter seems to be just a single 0x0E but that's hard to match.
	// Luckily all fields seem to be serialized with a fixed order, and we can match parts of the first "HasPassword" field along with it.
	sessionPrefix = []byte{0x0E, 0x00, 0x00, 0x00, 0x11, 0x00, 0x00, 0x00, 0x0B, 0x48, 0x61, 0x73}
)

type Sessions []Session

// SetMod allows to update all sessions with a single mod string.
func (s Sessions) SetMod(mod string) Sessions {
	for i := range s {
		s[i].Mod = mod
	}
	return s
}

type Session struct {
	SessionName        string `json:"session_name"`
	Mod                string `json:"mod"`
	MaximumPlayerCount int    `json:"maximum_player_count"`
	CurrentPlayerCount int    `json:"current_player_count"`
	GameMode           int    `json:"game_mode"`
	HasPassword        bool   `json:"has_password"`
	GlobalMapAssetId   string `json:"-"`
	Duration           int    `json:"-"`
	PlayerSeparation   bool   `json:"-"`

	// Network
	Hostname string `json:"-"`
	Port     int    `json:"-"`
	IPV6     string `json:"-"`

	// Proxy
	ProxySessionId string `json:"-"`
	ProxyHostname  string `json:"-"`
	ProxyPort      int    `json:"-"`
}

// BuildRequest there is still something wrong here, it works but uff.
// Joins Name + Version + Author
func BuildRequest(base string, mods []Mod) []byte {
	var builder bytes.Buffer
	builder.Write(ListPrefix)

	var s strings.Builder
	s.WriteString(base)
	s.WriteString(" ") // somehow always expected
	for _, mod := range mods {
		s.WriteString(mod.asStringForMultiplayer())
	}

	req := s.String()
	// Write the length of this string
	l := len(req)
	b := make([]byte, 4)
	binary.BigEndian.PutUint32(b, uint32(l))
	builder.Write(b)
	// Write the string
	builder.WriteString(req)
	// and the trailing 0x00
	builder.WriteByte(0)
	return builder.Bytes()
}

// ParseSessions tries to read the given buffer into a list of sessions.
func ParseSessions(b []byte) (Sessions, error) {
	if len(b) < len(SessionsPrefix)+4 {
		return nil, errors.New("input too short")
	}

	if !bytes.HasPrefix(b, SessionsPrefix) {
		return nil, errors.New("unexpected prefix for sessions list")
	}
	i := len(SessionsPrefix)

	// Read the number of sessions to expect
	n := int(binary.BigEndian.Uint32(b[i : i+4]))
	if n > 128 {
		return nil, errors.New("expected number of sessions too large, what the heck")
	} else if n == 0 {
		return nil, nil
	}

	// Quick scan for offsets
	i = 0
	offsets := make([]int, n)
	for s := 0; s < n; s++ {
		offset := bytes.Index(b[i:], sessionPrefix)
		if offset == -1 {
			return nil, errors.New("invalid offset when trying to parse session")
		}
		offsets[s] = i + offset
		// We don't know the end of this session but need to move the index for the next scan,
		// let's set it to at least the current offset and one session prefix.
		i += offset + len(sessionPrefix)
	}

	// Now parse them properly
	sessions := make([]Session, n)
	for o := range offsets {
		start := offsets[o]
		end := len(b)
		if o < len(offsets)-1 {
			end = offsets[o+1]
		}

		session, err := ParseSession(b[start:end])
		if err != nil {
			return nil, errors.Join(errors.New("failed to parse session"), err)
		}
		sessions[o] = session
	}

	return sessions, nil
}

func ParseSession(b []byte) (Session, error) {
	if len(b) < 1 {
		return Session{}, errors.New("input too short")
	}
	if b[0] != sessionPrefix[0] {
		return Session{}, errors.New("unexpected prefix")
	}

	// Educated guess for the map size :)
	entries := make(map[string][]byte, 11)
	for i := 1; i < len(b); {
		l := int(binary.BigEndian.Uint32(b[i : i+4]))
		i += 4

		k, v := parseEntry(b[i : i+l])
		entries[k] = v

		i += l
	}

	return sessionFromEntries(entries), nil
}

func parseEntry(b []byte) (string, []byte) {
	// Keys are always a string so let's start with that
	keyLength := int(binary.BigEndian.Uint32(b[0:4]))
	key := string(b[4 : 4+keyLength])
	// assert now a zero

	// The remainder is to be interpreted as value
	valueAt := 4 + keyLength + 1
	// assert there are any bytes left
	return key, b[valueAt:]
}

func sessionFromEntries(entries map[string][]byte) Session {
	var session Session

	for k, v := range entries {
		switch k {
		case "SessionName":
			session.SessionName = decodeString(v)
		case "GlobalMapAssetId":
			session.GlobalMapAssetId = decodeString(v)
		case "Hostname":
			s := decodeString(v)
			session.Hostname = applyHostnameFix(s)
		case "Port":
			session.Port = decodeInt(v)
		case "ProxyPort":
			session.ProxyPort = decodeInt(v)
		case "Duration":
			session.Duration = decodeInt(v)
		case "GameMode":
			session.GameMode = decodeInt(v)
		case "PlayerSeparation":
			session.PlayerSeparation = decodeBool(v)
		case "IPV6_Address":
			session.IPV6 = decodeString(v)
		case "HasPassword":
			session.HasPassword = decodeBool(v)
		case "MaximumPlayerCount":
			session.MaximumPlayerCount = decodeInt(v)
		case "CurrentPlayerCount":
			session.CurrentPlayerCount = decodeInt(v)
		case "ProxySessionId":
			session.ProxySessionId = decodeString(v)
		case "ProxyHostname":
			session.ProxyHostname = decodeString(v)
		default:
			panic("unmatched " + k)
		}
	}

	return session
}

// applyHostnameFix trims trailing 0x00 bytes that are somehow appended in the hostname field 🤷‍♂️
func applyHostnameFix(s string) string {
	return strings.TrimRightFunc(s, unicode.IsControl)
}

func decodeInt(v []byte) int {
	return int(binary.BigEndian.Uint32(v))
}

func decodeBool(v []byte) bool {
	return v[0] != 0
}

func decodeString(v []byte) string {
	l := int(binary.BigEndian.Uint32(v[0:4]))
	return string(v[4 : 4+l])
}
