package fuenf

import (
	"reflect"
	"testing"

	"emergencyx.gitlab.io/pkg/util"
)

func Test_applyHostnameFix(t *testing.T) {
	tests := []struct {
		name  string
		given string
		want  string
	}{
		{
			name:  "should accept empty names",
			given: "",
			want:  "",
		},
		{
			name:  "should trim trailing 0x00",
			given: "158.174.246.83\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000\u0000",
			want:  "158.174.246.83",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := applyHostnameFix(tt.given); got != tt.want {
				t.Errorf("applyHostnameFix() = %v, want %v", got, tt.want)
			}
		})
	}
}

var (
	equipmentMod = Mod{
		Name:    "equipment_modification",
		Version: "0.5.0",
		Author:  "DrDrummer",
	}
	lüdenscheidMod = Mod{
		Name:    "Emergency Luedenscheid",
		Version: "1.2.1",
		Author:  "Emergency Lüdenscheid Team",
	}
	wuppertalMod = Mod{
		Name:    "Feuerwehr Wuppertal",
		Version: "1.2.2.1",
		Author:  "Emergency Wuppertal",
	}
)

func TestBuildRequest(t *testing.T) {
	tests := []struct {
		name string
		base string
		mods []Mod
		want []byte
	}{
		{
			name: "base game only",
			base: Emergency5BaseString,
			want: util.MustDecode("800100010092ee7a4b00000012656d657267656e63795f355f342e322e302000"),
		},
		{
			name: "base game and Wuppertal",
			base: Emergency5BaseString,
			mods: []Mod{
				wuppertalMod,
			},
			want: util.MustDecode("800100010092ee7a4b0000003f656d657267656e63795f355f342e322e30204665756572776568722057757070657274616c312e322e322e31456d657267656e63792057757070657274616c00"),
		},
		{
			name: "base game and Lüdenscheid",
			base: Emergency5BaseString,
			mods: []Mod{
				lüdenscheidMod,
			},
			want: util.MustDecode("800100010092ee7a4b00000048656d657267656e63795f355f342e322e3020456d657267656e6379204c756564656e736368656964312e322e31456d657267656e6379204cc3bc64656e736368656964205465616d00"),
		},
		{
			name: "base game and Lüdenscheid and Equipment Mod",
			base: Emergency5BaseString,
			mods: []Mod{
				lüdenscheidMod,
				equipmentMod,
			},
			want: util.MustDecode("800100010092ee7a4b0000006c656d657267656e63795f355f342e322e3020456d657267656e6379204c756564656e736368656964312e322e31456d657267656e6379204cc3bc64656e736368656964205465616d65717569706d656e745f6d6f64696669636174696f6e302e352e3044724472756d6d657200"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := BuildRequest(tt.base, tt.mods); !reflect.DeepEqual(got, tt.want) {
				t.Log(tt.want)
				t.Log(got)
				t.Errorf("BuildRequest() = %v, want %v", got, tt.want)
			}
		})
	}
}
