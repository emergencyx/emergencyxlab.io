package fuenf

import "testing"

func TestMod_asStringForMultiplayer(t *testing.T) {
	t.Parallel()
	tests := []struct {
		name  string
		given Mod
		want  string
	}{
		{
			name: "should serialize mod",
			given: Mod{
				Name:    "equipment_modification",
				Version: "0.5.0",
				Author:  "DrDrummer",
			},
			want: "equipment_modification0.5.0DrDrummer",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.given.asStringForMultiplayer(); got != tt.want {
				t.Errorf("asStringForMultiplayer() = %v, want %v", got, tt.want)
			}
		})
	}
}
