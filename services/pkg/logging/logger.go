package logging

import (
	"fmt"
	"log/slog"
	"net/http"
	"strings"
)

// Access logs an access entry with call duration and status code
func Access(r *http.Request, statusCode int) {
	attr := access(r, statusCode, nil)

	var msg string
	if len(r.URL.RawQuery) == 0 {
		msg = fmt.Sprintf("%v ->%v %v", statusCode, r.Method, r.URL.Path)
	} else {
		msg = fmt.Sprintf("%v ->%v %v?...", statusCode, r.Method, r.URL.Path)
	}

	if statusCode >= 200 && statusCode <= 399 {
		slog.With(attr...).Info(msg)
	} else if statusCode >= 400 && statusCode <= 499 {
		slog.With(attr...).Warn(msg)
	} else {
		slog.With(attr...).Error(msg)
	}
}

// AccessError logs an error while accessing
func AccessError(r *http.Request, err error) {
	attr := access(r, 0, err)
	slog.With(attr...).Error("request failed")
}

func access(r *http.Request, statusCode int, err error) []any {
	url := r.URL.Path
	if r.URL.RawQuery != "" {
		url += "?" + r.URL.RawQuery
	}

	attributes := []any{
		slog.String("type", "access"),
		slog.String("remote", getRemoteIp(r)),
		slog.String("host", r.Host),
		slog.String("url", url),
		slog.String("method", r.Method),
		slog.String("correlation", GetCorrelationId(r.Header)),
	}

	if statusCode != 0 {
		attributes = append(attributes, slog.Int("status", statusCode))
	}

	if err != nil {
		attributes = append(attributes, slog.String("error", err.Error()))
	}

	return attributes
}

func getRemoteIp(r *http.Request) string {
	if r.Header.Get("X-Cluster-Client-Ip") != "" {
		return r.Header.Get("X-Cluster-Client-Ip")
	}
	if r.Header.Get("X-Real-Ip") != "" {
		return r.Header.Get("X-Real-Ip")
	}
	return strings.Split(r.RemoteAddr, ":")[0]
}
