package logging

import (
	"fmt"
	"net/http"
)

type LogMiddleware struct {
	Next http.Handler
}

func NewLogMiddleware(next http.Handler) *LogMiddleware {
	return &LogMiddleware{
		Next: next,
	}
}

func (mw *LogMiddleware) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	EnsureCorrelationId(r)

	defer func() {
		if rec := recover(); rec != nil {
			AccessError(r, fmt.Errorf("PANIC %v", rec))
		}
	}()

	lrw := &logResponseWriter{ResponseWriter: w}
	mw.Next.ServeHTTP(lrw, r)

	Access(r, lrw.statusCode)
}

type logResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func (lrw *logResponseWriter) Write(b []byte) (int, error) {
	if lrw.statusCode == 0 {
		lrw.statusCode = 200
	}
	return lrw.ResponseWriter.Write(b)
}

func (lrw *logResponseWriter) WriteHeader(statusCode int) {
	lrw.statusCode = statusCode
	lrw.ResponseWriter.WriteHeader(statusCode)
}
