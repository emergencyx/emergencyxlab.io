package history

import (
	"context"
	"database/sql"
	"slices"
	"strings"
	"sync"
	"time"

	sqldblogger "github.com/simukti/sqldb-logger"

	"emergencyx.gitlab.io/pkg/runtime"
	"emergencyx.gitlab.io/pkg/util"
)

const (
	createSessionsTable = `CREATE TABLE IF NOT EXISTS sessions ("id" INTEGER PRIMARY KEY AUTOINCREMENT,"created_at" datetime,"ended_at" datetime,"name" text,"mod" text,"started" bool,"protected" bool,"reachability" bool,"modem" bool,"players" int,"mode" int,"game" text) `
	createSession       = `INSERT INTO "sessions" ("created_at", "ended_at", "name", "mod", "started", "protected", "reachability", "modem", "players", "mode", "game") VALUES (?,?,?,?,?,?,?,?,?,?,?);`
	fetchSessionsByDate = `SELECT created_at, name, mod FROM sessions WHERE created_at >= ? AND created_at < ?;`

	// fetchStatistics generates a list of modifications
	// - for the interval [from, to)
	// - where each modification was seen at least 10 times
	// - but at most the top 10
	// - ordered by descending count of sessions
	rankingSubquery         = `WITH Ranked AS (SELECT mod, ROW_NUMBER() OVER(ORDER BY count(id) DESC) as rank FROM sessions WHERE created_at >= ? AND created_at < ? GROUP BY mod)`
	fetchStatistics         = rankingSubquery + " SELECT * FROM Ranked WHERE rank <= ?"
	fetchStatisticsPrevious = rankingSubquery + " SELECT * FROM Ranked WHERE mod in "

	// fetchMonthlyStatistics aggregates a list of modifications
	// - over all time :eyes:
	// - aggregates it by month
	// - returns any mod with at least 10 invocations
	fetchMonthlyStatistics = `SELECT mod, STRFTIME("%Y-%m", created_at) AS month, count(id) AS count FROM sessions WHERE created_at >= ? AND created_at < ? GROUP BY mod, STRFTIME("%Y-%m", created_at) HAVING count(id) >= 100 ORDER BY month, count DESC`

	fetchPlayerActivity = `SELECT STRFTIME("%Y-%m-%d %H", created_at) AS slot, count(id) AS count FROM sessions WHERE created_at >= ? AND created_at < ? GROUP BY STRFTIME("%Y-%m-%d %H", created_at) ORDER BY slot`

	fetchModActivity = `SELECT STRFTIME("%Y-%m-%d", created_at) AS slot, count(id) AS count FROM sessions WHERE created_at >= ? AND created_at < ? AND mod = ? GROUP BY STRFTIME("%Y-%m-%d", created_at) ORDER BY slot`
)

var _ SessionStorage = (*sqliteSessions)(nil)

type sqliteSessions struct {
	mu sync.Mutex
	db *sql.DB
}

func NewWithPersistent(file string) SessionStorage {
	db, err := sql.Open("sqlite3", file)
	if runtime.IsDebug() {
		db = sqldblogger.OpenDriver(file, db.Driver(), util.SQLLogAdapter{})
	}
	if err != nil {
		panic(err)
	}
	if _, err := db.Exec(createSessionsTable); err != nil {
		panic(err)
	}
	return &sqliteSessions{db: db}
}

func (s *sqliteSessions) GetLeaderboard(ctx context.Context, period time.Duration, to time.Time, n int) ([]*Leaderboard, error) {
	stmtCurrent, err := s.db.Prepare(fetchStatistics)
	defer stmtCurrent.Close()
	if err != nil {
		return nil, err
	}

	to = startOfDay(to)
	from := startOfDay(to.Add(-period))
	rows, err := stmtCurrent.QueryContext(ctx, from, to, n)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	// First, fetch the topN ranked entries in the [from, to]
	entries := make([]*Leaderboard, 0, n)
	titles := make([]any, 0, n)
	for rows.Next() {
		entry := new(Leaderboard)
		err = rows.Scan(&entry.Title, &entry.CurrentRank)
		if err != nil {
			return nil, err
		}
		entries = append(entries, entry)
		titles = append(titles, entry.Title)
	}

	// Then shift the interval back and find the rank for each title there
	to = from
	from = startOfDay(to.Add(-period))
	args := append([]any{from.Format(time.DateOnly), to.Format(time.DateOnly)}, titles...)
	fetchPrevious := prepareQueryWithPlaceholders(fetchStatisticsPrevious, len(titles))
	stmtPrevious, err := s.db.Prepare(fetchPrevious)
	defer stmtPrevious.Close()
	if err != nil {
		return nil, err
	}
	rows, err = stmtPrevious.QueryContext(ctx, args...)
	if err != nil {
		return nil, err
	}
	defer rows.Close()
	// Match the previous ranks to the current order
	for rows.Next() {
		var title string
		var rank int
		err = rows.Scan(&title, &rank)
		if err != nil {
			return nil, err
		}

		for _, entry := range entries {
			if entry.Title == title {
				entry.PreviousRank = rank
				break
			}
		}
	}

	return entries, nil
}

func (s *sqliteSessions) GetPlayerActivity(ctx context.Context, to time.Time) ([]Activity, error) {
	stmt, err := s.db.Prepare(fetchPlayerActivity)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}

	to = startOfDay(to).AddDate(0, 0, 1)
	from := to.AddDate(0, 0, -7)
	rows, err := stmt.QueryContext(ctx, from, to)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	m := make(map[string]*Activity)
	// Initialize right away to make sure it shows empty entries
	// if we are missing data later on.
	for t := from; t.Before(to); t = t.AddDate(0, 0, 1) {
		k := t.Format(time.DateOnly)
		m[k] = &Activity{Date: k, Activity: make([]int, 24)}
	}

	for rows.Next() {
		var slot string
		var count int
		err = rows.Scan(&slot, &count)
		if err != nil {
			return nil, err
		}
		t, err := time.Parse("2006-01-02 15", slot)
		if err != nil {
			return nil, err
		}
		k := t.Format(time.DateOnly)
		if _, has := m[k]; !has {
			continue
		}
		// Insert at the t'th hour
		m[k].Activity[t.Hour()] = count
	}

	stats := make([]Activity, 0, len(m))
	for _, statistics := range m {
		stats = append(stats, *statistics)
	}
	slices.SortFunc(stats, func(a, b Activity) int {
		return strings.Compare(a.Date, b.Date)
	})
	return stats, nil
}

func (s *sqliteSessions) GetStatistics(ctx context.Context, from, to time.Time) ([]MonthlyStatistics, error) {
	stmt, err := s.db.Prepare(fetchMonthlyStatistics)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}

	from = startOfMonth(from)
	to = startOfMonth(to).AddDate(0, 1, 0)

	rows, err := stmt.QueryContext(ctx, from, to)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	m := make(map[string]*MonthlyStatistics)
	for rows.Next() {
		var month string
		var stat Statistics
		err = rows.Scan(&stat.Mod, &month, &stat.Count)
		if err != nil {
			return nil, err
		}
		if _, has := m[month]; !has {
			m[month] = &MonthlyStatistics{Month: month}
		}
		m[month].Statistics = append(m[month].Statistics, stat)
	}

	stats := make([]MonthlyStatistics, 0, len(m))
	for _, statistics := range m {
		stats = append(stats, *statistics)
	}
	slices.SortFunc(stats, func(a, b MonthlyStatistics) int {
		return strings.Compare(a.Month, b.Month)
	})

	return stats, nil
}

func (s *sqliteSessions) GetStatisticsByMod(ctx context.Context, from, to time.Time, mod string) (ModActivity, error) {
	stmt, err := s.db.Prepare(fetchModActivity)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}

	a := startOfDay(from).Format(time.DateOnly)
	b := startOfDay(to).AddDate(0, 0, 1).Format(time.DateOnly)

	rows, err := stmt.QueryContext(ctx, a, b, mod)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	m := make(map[string]int)
	for rows.Next() {
		var date string
		var count int
		err = rows.Scan(&date, &count)
		if err != nil {
			return nil, err
		}
		m[date] = count
	}

	stats := make(ModActivity, 0, len(m))
	for date, count := range m {
		stats = append(stats, StatisticByMod{Date: date, Count: count})
	}
	slices.SortFunc(stats, func(a, b StatisticByMod) int {
		return strings.Compare(a.Date, b.Date)
	})
	return stats, nil
}

func (s *sqliteSessions) GetSessions(ctx context.Context, date time.Time) ([]Session, error) {
	from := date.Format(time.DateOnly)
	to := date.AddDate(0, 0, 1).Format(time.DateOnly)
	stmt, err := s.db.Prepare(fetchSessionsByDate)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}
	rows, err := stmt.QueryContext(ctx, from, to)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var sessions []Session
	for rows.Next() {
		var session Session
		err = rows.Scan(&session.CreatedAt, &session.Name, &session.Mod)
		if err != nil {
			return nil, err
		}
		sessions = append(sessions, session)
	}
	return sessions, nil
}

func (s *sqliteSessions) AddSession(ctx context.Context, session Session) (*Session, error) {
	session.ID = 0
	stmt, err := s.db.Prepare(createSession)
	defer stmt.Close()
	if err != nil {
		return nil, err
	}
	result, err := stmt.ExecContext(
		ctx,
		session.CreatedAt,
		session.EndedAt,
		session.Name,
		session.Mod,
		true,
		session.Password,
		session.Connectivity,
		true,
		session.PlayerCount,
		0,
		"em4",
	)
	if err != nil {
		return nil, err
	}

	id, err := result.LastInsertId()
	if err != nil {
		return nil, err
	}

	session.ID = id
	return &session, nil
}
