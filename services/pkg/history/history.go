package history

import (
	"context"
	"time"

	_ "github.com/mattn/go-sqlite3"
)

type Session struct {
	ID             int64 `json:"-"`
	CreatedAt      time.Time
	EndedAt        time.Time
	Name           string
	Mod            string
	PlayerCount    int
	MaxPlayerCount int
	Connectivity   bool
	Password       bool
}

// MonthlyStatistics holds entries of how many sessions were recorded within a month.
// The month is formatted as a date string in the shape "2006-01".
type MonthlyStatistics struct {
	Month      string       `json:"month"`
	Statistics []Statistics `json:"statistics"`
}

type StatisticByMod struct {
	Date  string `json:"date"`
	Count int    `json:"count"`
}
type ModActivity []StatisticByMod

// Statistics is a single entry of how many sessions were recorded within a certain timeframe.
type Statistics struct {
	Mod   string `json:"mod"`
	Count int    `json:"count"`
}

// Activity holds a date and at most 24 slots of activity for each hour of the day.
type Activity struct {
	Date     string `json:"date"`
	Activity []int  `json:"activity"`
}

// Leaderboard is a single entry in the leaderboard for one modification by title.
// It holds both the rank in the current reference frame (CurrentRank) compared to the last frame (PreviousRank).
type Leaderboard struct {
	Title        string `json:"title"`
	CurrentRank  int    `json:"current"`
	PreviousRank int    `json:"previous"`
}

type SessionStorage interface {
	// GetSessions retrieves all sessions for the given date.
	GetSessions(ctx context.Context, date time.Time) ([]Session, error)
	GetStatistics(ctx context.Context, from, to time.Time) ([]MonthlyStatistics, error)
	GetStatisticsByMod(ctx context.Context, from, to time.Time, mod string) (ModActivity, error)
	GetPlayerActivity(ctx context.Context, to time.Time) ([]Activity, error)
	// GetLeaderboard creates a leader board of the top n titles
	// based on the interval [to-period, to] and compares it to
	// the interval [to-period*2, to-period].
	GetLeaderboard(ctx context.Context, period time.Duration, to time.Time, n int) ([]*Leaderboard, error)
	AddSession(ctx context.Context, session Session) (*Session, error)
}

// startOfMonth aligns the given t to the start of the month.
func startOfMonth(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), 1, 0, 0, 0, 0, time.UTC)
}

// startOfDay aligns the given t to the start of the day.
func startOfDay(t time.Time) time.Time {
	return time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, time.UTC)
}

// prepareQueryWithPlaceholders appends n placeholders to query in the shape of "(?,?,...)".
func prepareQueryWithPlaceholders(query string, n int) string {
	if n < 1 {
		return query
	}
	query += "("
	for i := 0; i < n-1; i++ {
		query += "?,"
	}
	query += "?)"
	return query
}
