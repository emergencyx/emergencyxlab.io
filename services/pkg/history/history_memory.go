package history

import (
	"context"
	"time"
)

var _ SessionStorage = (*memorySessionStorage)(nil)

type memorySessionStorage struct {
	// 2006-01-02 => []Sessions
	sessions map[string][]Session
}

func NewForTestingInMemory(sessions []Session) SessionStorage {
	storage := make(map[string][]Session)
	for _, session := range sessions {
		k := session.CreatedAt.Format(time.DateOnly)
		storage[k] = append(storage[k], session)
	}
	return &memorySessionStorage{sessions: storage}
}

func (m *memorySessionStorage) GetLeaderboard(_ context.Context, _ time.Duration, _ time.Time, _ int) ([]*Leaderboard, error) {
	//TODO implement me
	panic("implement me")
}

func (m *memorySessionStorage) GetPlayerActivity(ctx context.Context, to time.Time) ([]Activity, error) {
	//TODO implement me
	panic("implement me")
}

func (m *memorySessionStorage) GetStatistics(_ context.Context, _, _ time.Time) ([]MonthlyStatistics, error) {
	//TODO implement me
	panic("implement me")
}

func (m *memorySessionStorage) GetStatisticsByMod(_ context.Context, _, _ time.Time, _ string) (ModActivity, error) {
	//TODO implement me
	panic("implement me")
}

func (m *memorySessionStorage) GetSessions(_ context.Context, date time.Time) ([]Session, error) {
	k := date.Format(time.DateOnly)
	return m.sessions[k], nil
}

func (m *memorySessionStorage) AddSession(ctx context.Context, session Session) (*Session, error) {
	k := session.CreatedAt.Format(time.DateOnly)
	m.sessions[k] = append(m.sessions[k], session)
	return &session, nil
}
