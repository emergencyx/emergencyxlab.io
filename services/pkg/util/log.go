package util

import (
	"context"
	"log/slog"

	sqldblogger "github.com/simukti/sqldb-logger"
)

type SQLLogAdapter struct{}

func (s SQLLogAdapter) Log(ctx context.Context, level sqldblogger.Level, msg string, data map[string]any) {
	lvl := slog.LevelInfo
	_ = lvl.UnmarshalText([]byte(level.String()))
	attrs := make([]slog.Attr, 0, len(data))
	for key, val := range data {
		attrs = append(attrs, slog.Any(key, val))
	}
	slog.LogAttrs(ctx, lvl, msg, attrs...)
}
