package util

import "encoding/hex"

// MustDecode helps to decode a hex string
func MustDecode(s string) []byte {
	b, err := hex.DecodeString(s)
	if err != nil {
		panic(err)
	}
	return b
}
