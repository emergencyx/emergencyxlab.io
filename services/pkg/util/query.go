package util

import (
	"errors"
	"fmt"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
)

const (
	monthOnly = "2006-01"
)

// QueryMonth tries to get and parse the query parameter k from c in a monthly format, e.g. 2006-01.
// If it fails to parse, it will return the current time instead.
func QueryMonth(c *gin.Context, k string) (time.Time, error) {
	parsed, err := time.Parse(monthOnly, c.Query(k))
	if err != nil {
		err = fmt.Errorf("failed to parse '%s' as monthly: %w", k, err)
		return time.Now(), err
	}
	return parsed, nil
}

// QueryDate tries to get and parse the query parameter k from c in a date format, e.g. 2006-01-02.
// If it fails to parse, it will return the current time instead.
func QueryDate(c *gin.Context, k string) (time.Time, error) {
	parsed, err := time.Parse(time.DateOnly, c.Query(k))
	if err != nil {
		err = fmt.Errorf("failed to parse '%s' as date: %w", k, err)
		return time.Now(), err
	}
	return parsed, nil
}

// QueryLimit tries to get and parse the query parameter k from c as an integer limit.
// If no parameter is set, it returns n. It will always validate the limit is not larger than m.
func QueryLimit(c *gin.Context, k string, n, m int) (int, error) {
	if s := c.Query(k); s != "" {
		p, err := strconv.Atoi(s)
		if err != nil {
			return 0, err
		}
		if p < 1 || p > m {
			err = errors.New(fmt.Sprintf("bad request, refusing to serve %d leaders for input '%s'", p, s))
			return 0, err
		}
		return p, nil
	}
	return min(n, m), nil
}
