package main

import (
	"bytes"
	"encoding/hex"
	"encoding/json"
	"log/slog"
	"math/rand"
	"net"
	"os"
	"time"

	"github.com/codecat/go-enet"

	"emergencyx.gitlab.io/api/fuenf_v1"
	"emergencyx.gitlab.io/pkg/fuenf"
	"emergencyx.gitlab.io/pkg/runtime"
)

const (
	matchmakingHost = "matchmaking.sixteen-tons.de"
	matchmakingPort = 42006
)

var discovery = []struct {
	base string
	mods []fuenf.Mod
}{
	// Base
	{
		base: fuenf.Emergency5BaseString,
	},
	// Emergency Lüdenscheid
	{
		base: fuenf.Emergency5BaseString,
		mods: []fuenf.Mod{
			{
				Name:    "Emergency Luedenscheid",
				Version: "1.2.1",
				Author:  "Emergency Lüdenscheid Team",
			},
		},
	},
}

func main() {
	runtime.SetupLogging()

	router := runtime.NewEngineWithDefaults()
	broadcast := fuenf.NewBroadcast()
	fuenf_v1.RegisterWithRouter(router.Group("/fuenf/v1"), broadcast)

	// Initialize enet
	enet.Initialize()

	f, err := os.OpenFile("packets.bin", os.O_RDWR|os.O_APPEND|os.O_CREATE, 0o666)
	defer f.Close()
	if err != nil {
		slog.Error("Failed to open binary log", "error", err)
	}

	// Create a client matchmakingHost
	client, err := enet.NewHost(nil, 1, 1, 0, 0)
	if err != nil {
		slog.Error("Couldn't create host for enet", "error", err)
		return
	}

	// Connect the client matchmakingHost to the server
	ips, err := net.LookupIP(matchmakingHost)
	if err != nil || len(ips) < 1 {
		slog.Error("Failed to lookup", "host", matchmakingHost, "error", err)
		return
	}
	rand.Shuffle(len(ips), func(i, j int) { ips[i], ips[j] = ips[j], ips[i] })
	ip := ips[0].String()
	slog.Info("Lookup for matchmaking", "found", len(ips), "ip", ip)

	peer, err := client.Connect(enet.NewAddress(ip, matchmakingPort), 2, 0xffffff)
	if err != nil {
		slog.Error("Couldn't connect", "error", err)
		return
	}

	logger := slog.With("component", "main")
	go runtime.Forever(router, logger)

	// The event loop
	var lastSent time.Time
eventLoop:
	for {
		// Wait until the next event
		ev := client.Service(uint32((30 * time.Second).Milliseconds()))

		switch ev.GetType() {
		case enet.EventConnect: // We connected to the server
			slog.Info("Connected to the server")

		case enet.EventDisconnect: // We disconnected from the server
			slog.Warn("Lost connection to the server, let's quit")
			break eventLoop

		case enet.EventReceive: // The server sent us data
			packet := ev.GetPacket()
			payload := packet.GetData()

			encoded := hex.EncodeToString(payload)
			f.WriteString(encoded + "\n")
			slog.Info("Received from server", "length", len(packet.GetData()), "payload", encoded)
			packet.Destroy()

			if bytes.Equal(payload, fuenf.EstablishIn) {
				slog.Debug("> 'establishIn'")
				err := peer.SendBytes(fuenf.EstablishReply, 0x00, enet.PacketFlagReliable)
				if err != nil {
					slog.Error("Failed to send EstablishReply", "error", err)
					continue
				}
			} else if bytes.HasPrefix(payload, fuenf.SessionsPrefix) {
				sessions, err := fuenf.ParseSessions(payload)
				ask := discovery[0]
				for _, mod := range ask.mods {
					sessions = sessions.SetMod(mod.Name)
				}

				if err != nil {
					slog.With("error", err).Error("failed to parse sessions")
				} else {
					s, _ := json.Marshal(sessions)
					slog.Info("Got sessions", "sessions", string(s), "duration", time.Since(lastSent).Milliseconds())
					ts := time.Now()
					broadcast.Announce(sessions)
					slog.With("duration", time.Since(ts).Milliseconds()).Debug("Broadcast done")
				}
			}
		case enet.EventNone:
			// For now: On request, shuffle the mods, request whatever is at position one, assign on response
			rand.Shuffle(len(discovery), func(i, j int) { discovery[i], discovery[j] = discovery[j], discovery[i] })

			ask := discovery[0]
			request := fuenf.BuildRequest(ask.base, ask.mods)
			slog.Debug("Ask for sessions", "mods", ask.mods, "request", hex.EncodeToString(request))
			err := peer.SendBytes(request, 0x00, enet.PacketFlagReliable)
			lastSent = time.Now()
			if err != nil {
				slog.Error("Failed to send EstablishReply", "error", err)
				continue
			}
		}

	}

	slog.Info("Shutting down, goodbye")

	// Destroy the matchmakingHost when we're done with it
	client.Destroy()

	// Uninitialize enet
	enet.Deinitialize()
}
