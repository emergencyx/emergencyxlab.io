meta:
  id: e4m
  file-extension: e4m
  endian: le
seq:
  - id: magic
    contents: [0x45, 0x4D, 0x33, 0x4D] #EM3M
  - id: unknown_intro
    contents: [0x03, 0x01, 0x00, 0x00]
  - id: weird
    type: u1
  - id: unknown_hm
    size: 7
  - id: unknown_hm_one_extra
    type: u1
    if: weird == 0x24
  - id: unknown_size_a
    type: s4
  - id: unknown_size_b
    type: s4
  - id: unknown_maybe_height
    size: 526338
  - id: texture_path
    type: em4_string_but_its_ascii_type
  - id: unknown_02010000
    size: 4
  # Then follows something that looks like vectors (z=-781)
  - id: outer_camera_bound_count
    type: s4
  - id: outer_camera_bound
    type: em4_vector
    repeat: expr
    repeat-expr: outer_camera_bound_count
  - id: unknown_two_vectors_and_then_something
    size: 32
  - id: inner_camera_bound_count
    type: s4
  - id: inner_camera_bound
    type: em4_vector
    repeat: expr
    repeat-expr: inner_camera_bound_count
  - id: again_unknown_two_vectors_and_then_something
    size: 28
  - id: object_count
    type: s4
  - id: prototype_file_count
    type: s4
  - id: prototype_files
    type: em4_string_type
    repeat: expr
    repeat-expr: prototype_file_count
  - id: objects
    type: em4_actor_type
    repeat: expr
    repeat-expr: object_count
  - id: liquid_count
    type: u4
  - id: liquids
    type: liquid
    repeat: expr
    repeat-expr: liquid_count
  - id: virtual_object_count
    type: u4
  - id: virtual_objects
    type: virtual_object
    repeat: expr
    repeat-expr: virtual_object_count
  - id: path_count
    type: u4
  - id: paths
    type: path
    repeat: expr
    repeat-expr: path_count

  - id: fire_object_count
    type: u4
  - id: fire_objects
    type: fire_object
    repeat: expr
    repeat-expr: fire_object_count

  - id: todo
    contents: [0, 0, 0, 0, 0, 0, 0, 0]

  - id: trigger_count
    type: u4
  - id: triggers
    type: trigger
    repeat: expr
    repeat-expr: trigger_count

  - id: street_count
    type: u4
  - id: streets
    type: street
    repeat: expr
    repeat-expr: street_count

  - id: spawn_point_count
    type: u4
  - id: spawn_points
    type: spawn_point
    repeat: expr
    repeat-expr: spawn_point_count
#  # Map properties go here
  - id: unknown_f
    size: 60
#  - id: timegradient
#    type: em4_string_but_its_ascii_type
#  - id: detail_polygon_count
#    type: u4
#  - id: stopping_points
#    type: stopping_point
#    repeat: expr
#    repeat-expr: stopping_point_count
  - id: unknown_g
    size: 40
#  - id: something_something_default
#    type: em4_string_but_its_ascii_type
#  - id: unknown_h
#    size: 4
#  - id: stopping_point_count
##    type: u4
#  - id: stopping_points
#    type: stopping_point
#    repeat: expr
#    repeat-expr: stopping_point_count

types:
  em4_string_type:
    doc: |
      Strings are prefixed with their length in characters and encoded
      using UTF-16, therefore every character is stored in two bytes.
    seq:
      - id: len
        type: u4
      - id: value
        type: str
        encoding: UTF-16LE
        size: len * 2
      - id: sentinel
        contents: [0x00, 0x00]
  em4_string_but_its_ascii_type:
    doc: |
      Except for when they are not, then they are just prefixed and
      the encoding is something like ASCII plus the final sentinel.
    seq:
      - id: len
        type: u4
      - id: value
        type: str
        encoding: ASCII
        size: len
      - id: sentinel
        contents: [0x00]
  em4_vector:
    seq:
      - id: x
        type: f4
      - id: y
        type: f4
      - id: z
        type: f4
  e4m_rotation_matrix:
    seq:
      - id: entries
        type: f4
        repeat: expr
        repeat-expr: 9

  em4_actor_type:
    doc: |
      An actor is the base type for all objects on the map.
      Specific properties are encoded by the traits section.
    seq:
      - id: actor_type
        type: s4
      - id: unknown_a
        size: 8
      - id: id
        type: s4
      - id: name
        type: em4_string_type
      - id: unknown_b
        size: 32
      - id: some_type
        type: em4_string_type
      - id: prototype_file
        type: em4_string_type
      # TODO: 0=none, 1=Car, 2=Truck, 8=Airplane
      - id: terrain_class
        type: s4
      - id: position
        type: em4_vector
      - id: rotation
        type: e4m_rotation_matrix
      - id: unknown_c
        size: 8
      - id: enabled_children_count
        type: s4
      - id: enabled_children
        type: s4
        repeat: expr
        repeat-expr: enabled_children_count
      - id: unknown_d
        size: 4
      - id: animation
        type: em4_string_but_its_ascii_type
      - id: command_count
        type: s4
      - id: commands
        type: em4_string_but_its_ascii_type
        repeat: expr
        repeat-expr: command_count
      - id: traits
        type:
          switch-on: actor_type
          cases:
            0x0002: trait_type_object
            0x0004: trait_type_house
            0x0008: trait_type_vehicle
            0x0010: trait_type_person
            0x2000: trait_type_open_house
  trait_type_none:
    seq:
      - id: no_value
        size: 0

  trait_type_object:
    seq:
      - id: unknown_a
        size: 12
      - id: unknown_count
        type: s4
      - id: unknowns
        type: s4
        repeat: expr
        repeat-expr: unknown_count
      - id: unknown_b
        size: 10
      - id: unknown_size
        type: s4
      - id: unknown_c_with_size
        size: 2 + unknown_size * 6
      - id: blocked_name
        type: em4_string_type
      - id: unknown_01010000
        contents: [1, 1, 0, 0]

  trait_type_house:
    seq:
      - id: unknown_a
        size: 12
      - id: disabled_light_count
        type: s4
      - id: disabled_lights
        type: s4
        repeat: expr
        repeat-expr: disabled_light_count
      - id: unknown_b
        size: 10
      - id: active_fire_children_count
        type: s4
      - id: unknown_active_fire_children
        size: 8 + active_fire_children_count * 6
      - id: unknown_01010000
        contents: [1, 1, 0, 0]

  trait_type_open_house:
    seq:
      - id: unknown_a
        size: 4
      - id: unknown_count
        type: s4
      - id: unknown_count_size_unknown_count
        type: s4
        repeat: expr
        repeat-expr: unknown_count
      - id: unknown_b
        size: 4
      - id: disabled_light_count
        type: s4
      - id: disabled_lights
        type: s4
        repeat: expr
        repeat-expr: disabled_light_count
      - id: unknown_c
        size: 10
      - id: active_fire_children_count
        type: s4
      - id: unknown_active_fire_children
        size: 8 + active_fire_children_count * 6
      - id: unknown_01010000
        contents: [1, 1, 0, 0]


  trait_type_vehicle:
    seq:
      - id: unknown_a
        size: 12
      - id: disabled_light_count
        type: s4
      - id: disabled_lights
        type: s4
        repeat: expr
        repeat-expr: disabled_light_count
      - id: unknown_b
        size: 26
      - id: energy
        type: f4
      - id: unknown_c
        size: 6
      - id: standard_path
        type: em4_string_type
      - id: escape_path
        type: em4_string_type
      - id: enclosed_path
        type: em4_string_type
      - id: resistance
        type: f4
      - id: smoke
        type: f4

  trait_type_person:
    seq:
      - id: unknown_a
        size: 12
      - id: disabled_light_count
        type: s4
      - id: disabled_lights
        type: s4
        repeat: expr
        repeat-expr: disabled_light_count
      - id: unknown_b
        size: 34
      - id: health
        type: f4
      - id: life
        type: f4
      - id: unknown_c
        size: 23
      - id: resistance_count
        type: s4
      - id: resistances
        type: float_key_value
        repeat: expr
        repeat-expr: resistance_count
      - id: injury_life_drain
        type: f4
      - id: contam_life_drain
        type: f4
      - id: medicate_life_gain
        type: f4
      - id: unknown_d
        size: 1
      - id: standard_path
        type: em4_string_type
      - id: escape_path
        type: em4_string_type
      - id: unknown_e
        size: 4
      - id: role
        type: u4
        enum: role
      - id: role_traits
        type:
          switch-on: role
          cases:
            'role::civil': person_type_civil
            'role::squad': person_type_squad
            'role::gangster': person_type_gangster
            'role::drowning': person_type_drowning
            'role::animal': person_type_animal

    types:
      person_type_civil:
        seq:
          - id: unknown_a
            size: 6
          - id: message_pool
            type: em4_string_but_its_ascii_type
          - id: unknown_b
            size: 9
      person_type_squad:
        seq:
          - id: unknown_a
            size: 1
          - id: function
            type: s4
          - id: unknown_b
            size: 1
          - id: message_pool
            type: em4_string_but_its_ascii_type
          - id: unknown_c
            size: 9
      person_type_gangster:
        seq:
          - id: unknown_a
            size: 1
          - id: reaction_range
            type: f4
          - id: shoot_range
            type: f4
          - id: spread
            type: f4
          - id: shoot_power
            type: f4
          - id: shoot_frequency
            type: s4
          - id: sight_angle
            type: f4
          - id: unknown_b
            size: 4
          - id: civils_flee_range
            type: f4
          - id: unknown_c
            size: 1
          - id: message_pool
            type: em4_string_but_its_ascii_type
          - id: unknown_d
            size: 9
      person_type_drowning:
        seq:
          - id: unknown_a
            size: 2
          - id: message_pool
            type: em4_string_but_its_ascii_type
          - id: unknown_b
            size: 9
      person_type_animal:
        seq:
          - id: unknown_a
            size: 6
          - id: message_pool
            type: em4_string_but_its_ascii_type
          - id: unknown_b
            size: 9
    enums:
      role:
        1: civil
        2: squad
        3: gangster
        4: drowning
        5: animal

  float_key_value:
    seq:
      - id: key
        type: s4
      - id: value
        type: f4

  liquid:
    seq:
      - id: magic1
        type: u4
      - id: unknown_a
        size: 8
      - id: name
        type: em4_string_type
      - id: unknown_b
        size: 32
      - id: point_count
        type: u4
      - id: points
        size: 40 + point_count * 12
      - id: texture
        type: em4_string_but_its_ascii_type
      - id: unknown_c
        size: 6


  virtual_object:
    seq:
      - id: unknown_a
        size: 12
      - id: name
        type: em4_string_type
      - id: unknown_b
        size: 28
      - id: height
        type: f4
      - id: terrain_type
        type: em4_string_but_its_ascii_type
      - id: unknown_c
        size: 4
      - id: point_count
        type: u4
      - id: points
        type: em4_vector
        repeat: expr
        repeat-expr: point_count
      # here follows extra data for the dust type etc.
      - id: unknown_two_vectors_and_then_something
        size: 31
      - id: dust_effect
        type: em4_string_but_its_ascii_type

  trigger:
    seq:
      - id: unknown_a
        size: 12
      - id: name
        type: em4_string_type
      - id: unknown_b
        size: 28
      - id: height
        type: f4
      - id: flags
        type: u4
      - id: trigger_by_name
        type: em4_string_type
      - id: unknown_c
        size: 4
      - id: point_count
        type: s4
      - id: points
        type: em4_vector
        repeat: expr
        repeat-expr: point_count
      - id: again_unknown_two_vectors_and_then_something
        size: 28

  street:
    seq:
      - id: unknown_a
        size: 12
      - id: name
        type: em4_string_type
      - id: unknown_b
        size: 32
      - id: point_count
        type: u4
      - id: points
        type: em4_vector
        repeat: expr
        repeat-expr: point_count
      - id: unknown_c
        size: 28
      - id: width
        type: f4
        repeat: expr
        repeat-expr: point_count - 1
      - id: unknown_00000000_or_01000000
        size: 4
  path:
    seq:
      - id: unknown_a
        size: 12
      - id: name
        type: em4_string_type
      - id: unknown_b
        size: 32
      - id: point_count
        type: u4
      - id: points
        type: em4_vector
        repeat: expr
        repeat-expr: point_count
      - id: closed
        type: u1
      # ff ff ff and then ca f2 49 71 repeated six times, no clue
      - id: unknown_c
        size: 27
      # loop, ping-pong, ...
      - id: mode
        type: u4
        enum: path_mode
      - id: width
        type: f4
        repeat: expr
        repeat-expr: point_count - 1
      - id: speed
        type: f4
      - id: path_mode_trait
        type:
          switch-on: mode
          cases:
            'path_mode::forward': path_mode_trait_forward
            'path_mode::backward': path_mode_trait_backward
            'path_mode::pingpong': path_mode_trait_pingpong
            'path_mode::loop': path_mode_trait_loop
    types:
      path_mode_trait_forward:
        seq:
          - id: unknown
            size: 1
      path_mode_trait_backward:
        seq:
          - id: unknown
            size: 1
      path_mode_trait_pingpong:
        seq:
          - id: unknown
            size: 5
      path_mode_trait_loop:
        seq:
          - id: unknown
            size: 1

    enums:
      path_mode:
        0: forward
        1: backward
        2: pingpong
        3: loop

  spawn_point:
    seq:
      - id: unknown_a
        size: 12
      - id: name
        type: em4_string_type
      - id: unknown_b
        size: 28
      # person, vehicle, object
      - id: spawn_type
        type: u4
      - id: interval
        type: f4
      - id: variance
        type: f4
      - id: unknown_c
        size: 2
      - id: target_path
        type: em4_string_type
      - id: escape_path
        type: em4_string_type
      - id: position
        type: em4_vector
      - id: unknown_d
        size: 4
      - id: spawn_point_entry_count
        type: u4
      - id: spawn_point_entries
        type: spawn_point_entry
        repeat: expr
        repeat-expr: spawn_point_entry_count
      - id: initial
        type: f4
      - id: unknown_e
        size: 4

  spawn_point_entry:
    seq:
      - id: prototype
        type: em4_string_type
      - id: probability
        type: f4

  stopping_point:
    seq:
      - id: unknown_a
        size: 12
      - id: name
        type: em4_string_type
      # Looks like this serializes some point data
      - id: unknown_b
        size: 172
      - id: path
        type: em4_string_type
      - id: unknown_c
        size: 5


  fire_object:
    seq:
      - id: unknown_a
        size: 12
      - id: name
        type: em4_string_type
      - id: unknown_b
        size: 48
      - id: material
        type: em4_string_but_its_ascii_type
      - id: unknown_c
        size: 4
      - id: small_effect_count
        type: u4
      - id: small_effects
        type: effect
        repeat: expr
        repeat-expr: small_effect_count
      - id: large_effect_count
        type: u4
      - id: large_effects
        type: effect
        repeat: expr
        repeat-expr: large_effect_count
    types:
      effect:
        seq:
          - id: material
            type: em4_string_but_its_ascii_type
          - id: unknown_c
            size: 24
