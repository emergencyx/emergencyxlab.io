package main

import (
	"log/slog"

	"emergencyx.gitlab.io/api/history_v1"
	"emergencyx.gitlab.io/api/history_v2"
	"emergencyx.gitlab.io/pkg/auth"
	"emergencyx.gitlab.io/pkg/history"
	"emergencyx.gitlab.io/pkg/runtime"
)

func main() {
	runtime.SetupLogging()

	router := runtime.NewEngineWithDefaults()
	storage := history.NewWithPersistent("matchmaking.db")
	history_v1.RegisterWithRouter(router.Group("/v1/sessions"), storage)

	authorizer := auth.WithAppToken(runtime.MustHaveEnv("APP_TOKEN"))
	authorizedGroup := router.Group("/v2/sessions", authorizer)
	history_v2.RegisterWithRouter(authorizedGroup, storage)

	logger := slog.With("component", "main")
	runtime.Forever(router, logger)
}
