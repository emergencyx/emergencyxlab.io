package main

import (
	"log/slog"

	"emergencyx.gitlab.io/pkg/runtime"
)

func main() {
	runtime.SetupLogging()

	router := runtime.NewEngineWithDefaults()
	logger := slog.With("component", "main")
	runtime.Forever(router, logger)
}
