package main

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	"emergencyx.gitlab.io/pkg/logging"
	"emergencyx.gitlab.io/pkg/login"
	"emergencyx.gitlab.io/pkg/runtime"
)

func main() {
	runtime.SetupLogging()

	config := login.ReadConfig()

	configToLog := *config
	configToLog.JwtSecret = "..."
	slog.With("config", configToLog).Info("Starting up")

	h, err := login.NewHandler(config)
	if err != nil {
		exit(nil, err)
	}

	handlerChain := logging.NewLogMiddleware(h)

	stop := make(chan os.Signal)
	signal.Notify(stop, syscall.SIGINT, syscall.SIGTERM)

	port := config.Port
	if port != "" {
		port = fmt.Sprintf(":%s", port)
	}

	httpSrv := &http.Server{Addr: port, Handler: handlerChain}

	go func() {
		if err := httpSrv.ListenAndServe(); err != nil {
			if errors.Is(err, http.ErrServerClosed) {
				slog.Info("server closed, shutting down")
			} else {
				exit(nil, err)
			}
		}
	}()
	sig := <-stop
	slog.Info("received shutdown signal, shutting down", "signal", sig)

	ctx, ctxCancel := context.WithTimeout(context.Background(), 5*time.Second)
	httpSrv.Shutdown(ctx)
	ctxCancel()
}

var exit = func(signal os.Signal, err error) {
	slog.Info("shutdown signal, shutting down", "signal", signal)
	if err == nil {
		os.Exit(0)
	} else {
		os.Exit(1)
	}
}
